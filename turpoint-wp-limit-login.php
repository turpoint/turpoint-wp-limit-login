<?php

/**
 * Plugin Name: WP Limit Login
 * Author: Turpoint
 * Author URI: https://turpoint.com
 * Description: A plugin for preventing brute-force attacks..
 * Version: 1.0.1
 * Requires at least: 5.0
 * Text Domain: turpoint-wp-limit-login
 */

require_once(dirname(__FILE__) . '/inc/class-limit-login.php');