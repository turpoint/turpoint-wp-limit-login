<?php

namespace WP_Limit_Login\Inc;

class WP_Limit_Login
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // Filters   
        add_filter('authenticate', [$this, 'authenticate'], 30, 3);

        // Hooks
        add_action('wp_login_failed', [$this, 'login_failed'], 10, 1);
    }

    /**
     * Authenticate
     */
    public function authenticate($user, $username, $password) 
    {
        if (get_transient('attempted_login')) {
            $datas = get_transient('attempted_login');
    
            if ($datas['tried'] >= 3) {
                $until = get_option('_transient_timeout_' . 'attempted_login');
                $time = $this->time_to_go($until);
    
                return new \WP_Error('too_many_tried',  sprintf(__('You have reached authentication limit, you will be able to try again in %1$s.'), $time));
            }
        }
    
        return $user;
    }

    /**
     * Login failed
     */
    public function login_failed($username)
    {
        if (get_transient('attempted_login')) {
            $datas = get_transient('attempted_login');
            $datas['tried']++;
    
            if ($datas['tried'] <= 3)
                set_transient('attempted_login', $datas , 300);
        } else {
            $datas = array(
                'tried'     => 1
           );
            set_transient('attempted_login', $datas , 300);
        }
    }

    /**
     * Time to go
     */
    public function time_to_go($timestamp)
    {
    
        // converting the mysql timestamp to php time
        $periods = array(
            "second",
            "minute",
            "hour",
            "day",
            "week",
            "month",
            "year"
       );
        $lengths = array(
            "60",
            "60",
            "24",
            "7",
            "4.35",
            "12"
       );
        $current_timestamp = time();
        $difference = abs($current_timestamp - $timestamp);
        for ($i = 0; $difference >= $lengths[$i] && $i < count($lengths) - 1; $i ++) {
            $difference /= $lengths[$i];
        }
        $difference = round($difference);
        if (isset($difference)) {
            if ($difference != 1)
                $periods[$i] .= "s";
                $output = "$difference $periods[$i]";
                return $output;
        }
    }
}

/**
 * Callback
 */
$limit_login = new WP_Limit_Login();