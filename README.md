# WP Limit Login

WordPress plugin for preventing brute-force attacks.

## Installation via composer

Add a reference to this repository in the `repositories` section of your projects `composer.json`:

```
"repositories" : {
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.com/turpoint/turpoint-wp-limit-login.git"
    },
    ...
}
```

And then require the package:

```
composer require turpoint/turpoint-wp-limit-login
```